import Utils.HibernateUtil;
import Utils.Parser;
import Utils.Util;
import model.ItemImpl;
import model.Page;
import org.jsoup.nodes.Element;

import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Shrey on 4/15/2014.
 */
public class RunApplication {

    public static void main( String[] args ) throws InterruptedException {
        final ExecutorService executorService = Executors.newFixedThreadPool(3);
        try {
            File dir = new File(RunApplication.class.getResource("/operatorlist").getPath());
            File[] files = dir.listFiles();
            HibernateUtil.createSessionFactory();
            for (final File file : files) {
                if (!file.getName().equals("bing.xml") && !file.getName().equals("cnn.xml"))
                executorService.execute(new Runnable() {
                    @Override
                    public void run() {
                            Element element = Util.getDocument(file.getName(),true).child(0);
                            Parser parser = new Parser(null);
                            try {
                                Page page = (Page) parser.createObject(element.attr("_type"));
                                parser.parseDocument( element, page, Util.getDocument(element.attr("url"),false));
                                ItemImpl.addItems(page.getItemList());
                            } catch (ClassNotFoundException e) {
                                e.printStackTrace();
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            } catch (InstantiationException e) {
                                e.printStackTrace();
                            }

                    }
                });
            }
        } catch (Exception e) {
                e.printStackTrace();
        }
        executorService.shutdown();
        boolean isWait = true;
        while (isWait)
        {
            if(executorService.isTerminated()){
                HibernateUtil.shutdown();
                isWait = !isWait;
            }
            else {
                Thread.sleep(6000);
            }
        }
    }
}