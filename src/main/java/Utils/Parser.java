package Utils;

import model.NewsLink;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Shrey on 5/30/2014.
 */
public class Parser {

    public static final String _content = "_content";
    public static final String _mapping = "_mapping";
    public static final String _type = "_type";
    public static final String _parser = "_parser";
    public static final String _collection = "_collection";
    public static final String _selector = "_selector";

    Object object;
    HashMap<String, String> classMap = new HashMap<>();

    public Parser(Object object) {
        this.object = object;
        classMap.put("Page", "model.Page");
        classMap.put("Item", "model.Item");
        classMap.put("NewsLink", "model.NewsLink");
        classMap.put("String", "java.lang.String");
    }

    public Long FoxDuration(String duration, String rssLink, Element element) {
        Pattern p = Pattern.compile("([0-9]{1,2}:[0-9]{2}:[0-9]{2})|([0-9]{1,2}:[0-9]{2})");
        Matcher m = p.matcher(duration);
        long secs = 0;
        if (m.find()) {
//            System.out.println(m.start() + " " + m.end());
            String s= duration.substring(m.start(),m.end());
            String[] values = s.split(":");
            for (int i = values.length - 1; i >= 0; i--) {
                double value = Long.parseLong(values[i]);

                secs += value * Math.pow(60, values.length - 1 - i);
            }
        }
        return secs;
    }

    public Long AbcDuration(String duration, String rssLink, Element element) {
        return java.time.Duration.parse("P"+duration).getSeconds();

    }

    public Object createObject(String type) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        Object object = null;
        object = Class.forName(classMap.get(type)).newInstance();
        return object;
    }

    public void parseDocument(Element element, Object currentObject, Element resourceDocument)
            throws ClassNotFoundException, IllegalAccessException,
            InstantiationException {

        Element firstElement = element.child(0);
        while (firstElement != null) {
            String FirstElementTagName = firstElement.tagName();
            String firstElementType = firstElement.attr(_type);
            String collection = firstElement.attr(_collection);
            String parser = firstElement.attr(_parser);
            int numberOfTimes = 1;
            Elements correspondingElements = resourceDocument.getElementsByTag(FirstElementTagName);
            correspondingElements = (correspondingElements.size() == 0 ?
                    resourceDocument.getElementsByTag(element.tagName()) : correspondingElements);

            if (collection.equals("1")) {
                numberOfTimes = correspondingElements.size();
            }
            for (int i = 0; i < numberOfTimes; i++) {
                if (firstElement.childNodeSize() > 0) {
                    Object firstElementObject = firstElementType.equals("String") ? correspondingElements.get(i).text() : createObject(firstElementType);

                    invokeSetterMethod(currentObject.getClass().getName(), currentObject,
                            "set" + firstElement.attr(_mapping), classMap.get(firstElementType), firstElementObject);

                    if (!parser.isEmpty()) {
                        String parserMethodName = firstElement.attr(_parser);
                        String link = correspondingElements.text();

                        Method method = null;
                        try {
                            method = this.getClass().getDeclaredMethod(parserMethodName, new Class[]{Element.class, String.class, firstElementObject.getClass()});
                            method.invoke(this, firstElement, link, firstElementObject);
                        } catch (NoSuchMethodException e) {
                            e.printStackTrace();
                        } catch (InvocationTargetException e) {
                            e.printStackTrace();
                        }

                    } else {
                        Object nonStringParentObject = firstElementObject;
                        if (firstElementType.equals("String")) {
                            nonStringParentObject = currentObject;
                        }
                        parseDocument(firstElement, nonStringParentObject,
                                (correspondingElements.isEmpty()) ?
                                        resourceDocument : correspondingElements.get(i));
                    }

                } else {
                    String text = null;
                    if (!firstElement.attr(_content).isEmpty()) {
                        text = firstElement.attr(_content);
                    } else {
                        text = correspondingElements.get(i).text();
                    }
                    invokeSetterMethod(currentObject.getClass().getName(), currentObject,
                            "set" + firstElement.attr(_mapping), classMap.get(firstElementType), text);
                }
            }


            firstElement = firstElement.nextElementSibling();
        }
    }

    public void DefaultParser(Element newsLinkElement, String link, NewsLink newsLink) throws IllegalAccessException, InstantiationException, ClassNotFoundException {

        Document videoDocument = Util.getDocument(link, false);
        for (Element e : newsLinkElement.children()
                ) {
            String eTagName = e.tagName();
            boolean contentTagIsThere = e.hasAttr(_content);
            String contentInE = e.attr(_content);
            boolean contentValueIsEmpty = contentInE.isEmpty();
            Elements metaElement;

            if (!contentValueIsEmpty) {
                invokeSetterMethod(newsLink.getClass().getName(), newsLink,
                        "set" + e.attr(_mapping), classMap.get(e.attr(_type)),
                        e.attr(_content));
            } else if (!e.attr(_parser).isEmpty()) {
                String parserMethodName = e.attr(_parser);
                String toParse;
                if (contentTagIsThere && !contentValueIsEmpty) {
                    toParse = contentInE;
                } else {
                    //_content="" in abc.xml, so taking from videodocument content attribute
                    if (contentTagIsThere) {
                        Elements temp = videoDocument.select(e.attr(_selector));
                        toParse = temp.first().attr("content");
                    } else toParse = videoDocument.select(e.attr(_selector)).text();

                }
                Object object = createObject(e.attr(_type));

                Method method = null;
                try {
                    method = this.getClass().getDeclaredMethod(parserMethodName, new Class[]{String.class, String.class, Element.class});
                    object = method.invoke(this, toParse, link, newsLinkElement);
                    invokeSetterMethod(newsLink.getClass().getName(), newsLink,
                            "set" + e.attr(_mapping), object.getClass().getName(),
                            object);
                } catch (NoSuchMethodException e1) {
                    e1.printStackTrace();
                } catch (InvocationTargetException e1) {
                    e1.printStackTrace();
                } catch (IllegalAccessException e1) {
                    e1.printStackTrace();
                }
            } else {
                String selectionMentioned = e.attr(_selector);
                if (!selectionMentioned.isEmpty()) {
                    metaElement = videoDocument.select(selectionMentioned);
                } else {
                    Attributes attributeList = e.attributes();
                    Iterator<Attribute> i = attributeList.iterator();
                    StringBuilder selector = new StringBuilder();
                    selector.append(eTagName);
                    while (i.hasNext()) {
                        Attribute attr = i.next();
                        if (!(attr.getKey().startsWith("_"))) {
                            selector.append("[" + attr.getKey() + "=" + attr.getValue() + "]");
                        }
                    }
                    metaElement = videoDocument.select(selector.toString());
                }
                invokeSetterMethod(newsLink.getClass().getName(), newsLink,
                        "set" + e.attr(_mapping), classMap.get(e.attr(_type)),
                        contentTagIsThere ? metaElement.first().attr("content") : metaElement.first().text());
            }
        }
    }

    public String CnnParser(String nonuseFullContent, String link, Element element) {
        StringBuilder linkBuilder = new StringBuilder();
        linkBuilder.append("http://cnn-f.akamaihd.net/control/cnn/big/");
        Pattern p = Pattern.compile("http://www.cnn.com/video/#/video(.+?)");
        Matcher m = p.matcher(link);
        if (m.find()) {
            linkBuilder.append(link.substring(m.end()));
            linkBuilder.append("__,512x288_55,640x360_90,768x432_130,896x504_185,1280x720_350,0k.mp4.csmil_0_0@3?cmd=throttle,90&v=3.4.1.1");
            //  http://cnn-f.akamaihd.net/control/cnn/big/entertainment/2015/12/31/making-a-murderer-series-darren-kavinoky-justice-system-cnni-nr-intv.cnn

        }


        return linkBuilder.toString();
    }


// <editor-fold desc="Description">
//        org.jsoup.nodes.Document doc = Jsoup.connect(link).timeout(0).get();
//        video.setHtmlSrcNewsLink(doc.html());

//        for (int i = 0; i < nodeList.getLength(); i++) {
//            Node node = nodeList.item(i);
//            if (node instanceof Element) {
//                String nodeName = ((Element) node).getNodeName();
//                String contentToSave = "";
//                if (nodeName.equals("meta")) {
//                    contentToSave = getContentFromMetaTag(node, doc);
////                    System.out.println(contentToSave);
//                } else if (node.getAttributes().getLength() != 0) {
//                    String tempAttribute = node.getAttributes().item(0).getNodeName();
//                    Elements links = doc.select(node.getNodeName() + "[" + tempAttribute + "]"); // a with href
//                    String attr = links.attr(tempAttribute);
//                    contentToSave = attr;
//                } else {
//                    Elements links = doc.select(node.getNodeName()); // a with href
//                    contentToSave = links.get(0).text();
//                }
//                invokeMethod(video, "set" + node.getTextContent(), contentToSave);
//            }
//        }


    //Document rssDocument = Util.getDocument(page.attr("url"), false);

//        if(siteName.equals("abc.xml"))
//        for (Element element : resourceDocument.getElementsByTag("RssItem").first().children()
//                ) {
//            System.out.println(element.nodeName());
//
//        }

//        Document rssDocument = getDocument(resourceDocument.getElementsByTagName("url").item(0).getTextContent(), false);
//        String startTag = resourceDocument.getElementsByTagName("startTag").item(0).getTextContent();
//        NodeList rssLinksList = rssDocument.getElementsByTagName(startTag);
//        NodeList nodeListProperties = resourceDocument.getElementsByTagName("RssItem").item(0).getChildNodes();

//        for (int i = 0; i < rssLinksList.getLength(); ++i) {
//            NodeList node = rssLinksList.item(i).getChildNodes();
//            NewsLink  = new NewsLink();
//            video.setNewsSrc(siteName.substring(0, siteName.indexOf('.')));
//            for (int j = 0; j < nodeListProperties.getLength(); j++) {
//                Node property = nodeListProperties.item(j);
//                if (property instanceof Element) {
//                        invokeMethod(video,"set" + property.getTextContent(),getNodeByName(node, property.getNodeName()).getTextContent());
////                    if(property.getTextContent().equalsIgnoreCase("title")) System.out.println(getNodeByName(node, property.getNodeName()).getTextContent());
////                    System.out.println("set" + property.getTextContent()+" "+getNodeByName(node, property.getNodeName()).getTextContent());
//                    if (property.getNodeName().equalsIgnoreCase(resourceDocument.getElementsByTagName("linkTag").item(0).getTextContent())) {
//                        try {
//                            Method methods = this.getClass().getDeclaredMethod(videoParser, String.class, NewsLink.class, NodeList.class);
//                            methods.invoke(this, getNodeByName(node, property.getNodeName()).getTextContent(), video, resourceDocument.getElementsByTagName("video").item(0).getChildNodes());
//                        } catch (Exception e) {
//                            System.out.println("Problem calling parser method eg cnnParser");
//                            e.printStackTrace();
//                        }
//                    }
//                }
//            }
//            newsLinkList.add(video);
//        }
//        return newsLinkList;


//    public void bingVideoParser(String link, NewsLink video, NodeList nodeList) throws IOException {
//        org.jsoup.nodes.Document doc = Jsoup.connect(link).timeout(0).get();
//        video.setHtmlSrcNewsLink(doc.html());
//
//        for (int i = 0; i < nodeList.getLength(); i++) {
//            Node node = nodeList.item(i);
//            if (node instanceof Element) {
//                String nodeName = ((Element) node).getNodeName();
//                String contentToSave = "";
//                if (nodeName.equals("meta")) {
//                    contentToSave = getContentFromMetaTag(node, doc);
////                    System.out.println(contentToSave);
//                } else if (node.getAttributes().getLength() != 0) {
//                    String tempAttribute = node.getAttributes().item(0).getNodeName();
//                    Elements links = doc.select(node.getNodeName() + "[" + tempAttribute + "]"); // a with href
//                    String attr = links.attr(tempAttribute);
//                    contentToSave = attr;
//                } else {
//                    Elements links = doc.select(node.getNodeName()); // a with href
//                    contentToSave = links.get(0).text();
//                }
//                invokeMethod(video, "set" + node.getTextContent(), contentToSave);
//            }
//        }
//    }
//
//    public void googleVideoParser(String link, NewsLink video, NodeList nodeList) throws IOException {
//        org.jsoup.nodes.Document doc = Jsoup.connect(link).timeout(0).get();
//        video.setHtmlSrcNewsLink(doc.html());
//
//        for (int i = 0; i < nodeList.getLength(); i++) {
//            Node node = nodeList.item(i);
//            if (node instanceof Element) {
//                String nodeName = ((Element) node).getNodeName();
//                String contentToSave = "";
//                if (nodeName.equals("meta")) {
//                    contentToSave = getContentFromMetaTag(node, doc);
////                    System.out.println(contentToSave);
//                } else if (node.getAttributes().getLength() != 0) {
//                    String tempAttribute = node.getAttributes().item(0).getNodeName();
//                    Elements links = doc.select(node.getNodeName() + "[" + tempAttribute + "]"); // a with href
//                    String attr = links.attr(tempAttribute);
//                    contentToSave = attr;
//                } else {
//                    Elements links = doc.select(node.getNodeName()); // a with href
//                    contentToSave = links.get(0).text();
//                }
//                invokeMethod(video, "set" + node.getTextContent(), contentToSave);
//            }
//        }
//    }
//
//
//
//    public void cnnVideoParser(String link, NewsLink video, NodeList nodeList) throws IOException {
//
//        String addModifier = "data/2.0";
//        link = link.replace("#", addModifier) + ".html";
//        org.jsoup.nodes.Document doc = Jsoup.connect(link).timeout(0).get();
//        video.setHtmlSrcNewsLink(doc.html());
//        for (int i = 0; i < nodeList.getLength(); i++) {
//            Node node = nodeList.item(i);
//            if (node instanceof Element) {
//                Elements temp = doc.getElementsByTag(((Element) node).getTagName());
//                String nodeName = ((Element) node).getNodeName();
//                String contentToSave = "";
//                if (nodeName.equals("meta")) {
//                    contentToSave = getContentFromMetaTag(node, doc);
//                } else if (node.getAttributes().getLength() != 0) {
//                    String tempAttribute = node.getAttributes().item(0).getNodeName();
//                    Elements links = doc.select(node.getNodeName() + "[" + tempAttribute + "]"); // a with href
//                    String attr = links.attr(tempAttribute);
//                    contentToSave = attr;
//                } else {
//
//                    Elements links = doc.select(node.getNodeName()); // a with href
//                    contentToSave = links.get(0).text();
//                }
//                invokeMethod(video, "set" + node.getTextContent(), contentToSave);
//            }
//        }
//    }
//
//    public String getContentFromMetaTag(Node node, org.jsoup.nodes.Document doc) {
//        String contentToSave = "";
//        String search = node.getNodeName();
//        String attributeToSave = "";
//        for (int j = 0; j < node.getAttributes().getLength(); j++) {
//            String tempAttributeName = node.getAttributes().item(j).getNodeName();
//            String tempAttributeContent = node.getAttributes().item(j).getNodeValue();
//            if (tempAttributeContent.equals("")) attributeToSave = tempAttributeName;
//            else
//                search += "[" + tempAttributeName + "=" + tempAttributeContent + "]";
//        }
//        Elements links = doc.select(search); // a with href
//        contentToSave = links.attr(attributeToSave);
//
//        for (int j = 1; j < links.size(); j++) {
//            contentToSave += ","+links.get(j).attr(attributeToSave);
//        }
//        return contentToSave;
//    }
//
//    public void foxVideoParser(String link, NewsLink video, NodeList nodeList) throws IOException {
//        org.jsoup.nodes.Document doc = Jsoup.connect(link).timeout(0).get();
//        video.setHtmlSrcNewsLink(doc.html());
//
//        for (int i = 0; i < nodeList.getLength(); i++) {
//            Node node = nodeList.item(i);
//            if (node instanceof Element) {
//                String nodeName = ((Element) node).getNodeName();
//                String contentToSave = "";
//                if (nodeName.equals("meta")) {
//                    contentToSave = getContentFromMetaTag(node, doc);
//                } else if (node.getAttributes().getLength() != 0) {
//                    String tempAttribute = node.getAttributes().item(0).getNodeName();
//                    Elements links = doc.select(node.getNodeName() + "[" + tempAttribute + "]"); // a with href
//                    String attr = links.attr(tempAttribute);
//                    contentToSave = attr;
//                } else {
//                    Elements links = doc.select(node.getNodeName()); // a with href
//                    contentToSave = links.get(0).text();
//                }
//                invokeMethod(video, "set" + node.getTextContent(), contentToSave);
//            }
//        }
//    }
//

//
//    public Node getNodeByName(NodeList nodeList, String tagName) {
//        for (int i = 0; i < nodeList.getLength(); i++) {
//            Node node = nodeList.item(i);
//            if (node instanceof Element && node.getNodeName().equalsIgnoreCase(tagName)) {
//                return node;
//            }
//        }
//        return null;
//    }

//    public ArrayList<Map<String, String>> nodesToList(NodeList nodeList) {
//        ArrayList<Map<String, String>> finalList = new ArrayList<>();
//        for (int i = 0; i < nodeList.getLength(); i++) {
//            NodeList nodeList1 = nodeList.item(i).getChildNodes();
//            Map<String, String> temp = new HashMap<>();
//            for (int j = 0; j < nodeList1.getLength(); j++) {
//                Node node = nodeList1.item(j);
//                if (node instanceof Element) {
//                    Element docElement = (Element) node;
//                    temp.put(docElement.getTagName(), docElement.getTextContent());
//                }
//            }
//            finalList.add(temp);
//        }
//        return finalList;
//    }
    // </editor-fold>

    public void invokeSetterMethod(String parentClassName, Object parentObject, String methodName, String objectName, Object object) {
        Method methods = null;
        try {
            Class parentClass = Class.forName(parentClassName);
            Class objectClass = Class.forName(objectName);
            methods = parentClass.getDeclaredMethod(methodName, new Class[]{objectClass});
            methods.invoke(parentObject, (objectClass.cast(object)));
        } catch (NoSuchMethodException e) {
            System.out.println("Not Found Method: " + methodName);
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            System.out.println("Not Found Class: " + parentClassName);
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public String invokeGetterMethod(String parentClassName, Object parentObject, String methodName) {
        Method methods = null;
        try {

            methods = Class.forName(parentClassName).getDeclaredMethod(methodName, new Class[]{String.class});
            return (String) methods.invoke(parentObject);

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return parentClassName;
    }


}
