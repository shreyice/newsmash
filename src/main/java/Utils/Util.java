package Utils;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Shrey on 12/26/2015.
 */
public class Util {

    public static Document getDocument(String link, boolean local) {
        InputStream inputStream;
        try {
            if (local)
                inputStream = new BufferedInputStream(ClassLoader.getSystemResourceAsStream(link));
            else
                inputStream = new BufferedInputStream(new URL(link).openStream());
            Document doc = Jsoup.parse(inputStream, "ISO-8859-1", link, org.jsoup.parser.Parser.xmlParser());
            inputStream.close();
            return doc;
        } catch (Exception e) {
            if (e instanceof IOException) {
                System.out.println("Problem opening stream");

            } else if (e instanceof MalformedURLException) {
                System.out.println("Improper URL");
            }
            e.printStackTrace();
        }
        return null;
    }

}
