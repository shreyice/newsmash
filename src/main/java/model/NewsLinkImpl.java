package model;

import Utils.HibernateUtil;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.TransactionException;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by Shrey on 12/30/2015.
 */
public class NewsLinkImpl {

    public static void addNewsLinks(List<NewsLink> collection) {

        Session session = null;
        Transaction tx = null;
        try {
            System.out.println(HibernateUtil.getSessionFactory());
            session = HibernateUtil.getSessionFactory().openSession();
            try {

                tx = session.beginTransaction();
                tx.setTimeout(5);
                for (int i = 0; i < collection.size(); i++) {
                    NewsLink newsLink = collection.get(i);

                    Criteria criteria = session.createCriteria(Item.class)
                            .add(Restrictions.eq("title", newsLink.getTitle()));
                    List out = criteria.list();
                    if (out.isEmpty()) {
                        session.persist(newsLink);
                    } else {
                        System.out.println("Exists " + newsLink.getTitle() + newsLink.getSrc());
                    }
                }
                tx.commit();
            } catch (TransactionException e) {
                System.out.println(e.getMessage());
            }
        } catch (RuntimeException e) {
            System.out.println(e.getMessage());
            tx.rollback();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }


    public static void addNewsLink(Item rssItem) {

        Session session = null;
        Transaction tx = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            tx.setTimeout(5);
            Criteria criteria = session.createCriteria(Item.class)
                    .add(Restrictions.eq("title", rssItem.getTitle()));
            List videos = criteria.list();
            if (videos.isEmpty()) {
                session.persist(rssItem);
            } else {
                System.out.println("Exists " + rssItem.getTitle());
            }

            tx.commit();
        } catch (RuntimeException e) {
            tx.rollback();
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }


    public static void displayNewsLink() {

        Session session = null;
        Transaction tx = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            Criteria criteria = session.createCriteria(NewsLink.class)
                    .setProjection(Projections.projectionList().add(Projections.property("title")));
            List titles = criteria.list();
            tx.setTimeout(5);
            for (int i = 0; i < titles.size(); i++) {
                System.out.println(i + "  " + titles.get(i));
            }
            tx.commit();

        } catch (RuntimeException e) {
            e.printStackTrace();
            tx.rollback();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }
}
