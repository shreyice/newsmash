package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Shrey on 12/26/2015.
 */
public class Page implements Serializable{

    private String src;

    private List<Item> itemList;

    public Page() {
        itemList = new ArrayList<>();
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public List<Item> getItemList() {
        return itemList;
    }

    public void setItemList(List<Item> itemList) {
        this.itemList = itemList;
    }

    public void setItem(Item item) {
        this.itemList.add(item);
    }
}
