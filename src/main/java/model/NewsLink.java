package model;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Duration;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Shrey on 4/15/2014.
 */

@Entity
@Table(name = "newsLink")
public class NewsLink implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ManyToOne
    @JoinColumn(name = "item_id")
    private Item item;

    @Column(name = "title")
    private String title;

    @Column(name = "video_url_swf", unique = true)
    private String videoUrlSwf;

    @Column(name = "video_url_mp4", unique = true)
    private String videoUrlMp4;

    @Column(name = "category")
    private String category;

    @Column(name = "keywords")
    private String keywords;

    @Column(name = "description")
    private String description;

    @Column(name = "pub_date")
    @Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime publicationDate;

    @Column(name = "duration")
    private long duration;

    @Column(name = "news_link_type", nullable = false )
    private String type;

    @Column(name = "html_src")
    private String htmlSrcNewsLink;

    @Column (name = "src")
    private String src;

    public String getHtmlSrcNewsLink() {
        return htmlSrcNewsLink;
    }

    public void setHtmlSrcNewsLink(String htmlSrcNewsLink) {
        this.htmlSrcNewsLink = htmlSrcNewsLink;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public DateTime getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(String publicationDate) {

        DateTime dt;
        DateTimeFormatter formatter;
        try {
            formatter = DateTimeFormat.forPattern("EEE, dd MMM yyyy HH:mm:ss zzz");
            dt = formatter.parseDateTime(publicationDate);

        } catch (Exception e) {
            formatter = DateTimeFormat.forPattern("EEE, dd MMM yyyy HH:mm:ss Z");
            dt = formatter.parseDateTime(publicationDate);
        }
        this.publicationDate = dt.withZone(DateTimeZone.UTC);

    }

    public void setPublicationDate(DateTime publicationDate) {
        this.publicationDate = publicationDate.withZone(DateTimeZone.UTC);
    }

    public String getVideoUrlSwf() {
        return videoUrlSwf;
    }

    public void setVideoUrlSwf(String videoUrlSwf) {
        this.videoUrlSwf = videoUrlSwf;
    }

    public String getVideoUrlMp4() {
        return videoUrlMp4;
    }

    public void setVideoUrlMp4(String videoUrlMp4) {
        this.videoUrlMp4 = videoUrlMp4;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }
}
