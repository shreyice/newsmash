package model;

import Utils.HibernateUtil;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.TransactionException;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by Shrey on 12/30/2015.
 */
public class ItemImpl {
    
    public static void addItems(List<Item> collection) {

        Session session = null;
        Transaction tx = null;
        try {
            session = HibernateUtil.getSessionFactory().withOptions().interceptor(new AuditInterceptor()).openSession();

            try {

                tx = session.beginTransaction();
                tx.setTimeout(5);
                for (int i = 0; i < collection.size(); i++) {
                    Item rssItem = collection.get(i);

                    Criteria criteria = session.createCriteria(Item.class)
                            .add(Restrictions.eq("title", rssItem.getTitle()));
                    List out = criteria.list();
                    if (out.isEmpty()) {
                        session.saveOrUpdate(rssItem);
                    } else {
                        System.out.println("Exists " + rssItem.getTitle() + rssItem.getSrc());
                    }
                }
                tx.commit();
            } catch (TransactionException e) {
                System.out.println(e.getMessage());
            }
        } catch (RuntimeException e) {
            System.out.println(e.getMessage());
            tx.rollback();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }


    public static void addItem(Item rssItem) {

        Session session = null;
        Transaction tx = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            tx.setTimeout(5);
            Criteria criteria = session.createCriteria(Item.class)
                    .add(Restrictions.eq("title", rssItem.getTitle()));
            List videos = criteria.list();
            if (videos.isEmpty()) {
                System.out.println("Enter " + rssItem.getTitle());
                session.saveOrUpdate(rssItem);
            } else {
                System.out.println("Exists " + rssItem.getTitle());
            }

            tx.commit();
        } catch (RuntimeException e) {
            tx.rollback();
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }

    }


    public static void displayItem() {

        Session session = null;
        Transaction tx = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            Criteria criteria = session.createCriteria(NewsLink.class)
                    .setProjection(Projections.projectionList().add(Projections.property("title")));
            List titles = criteria.list();
            tx.setTimeout(5);
            for (int i = 0; i < titles.size(); i++) {
                System.out.println(i + "  " + titles.get(i));
            }
            tx.commit();

        } catch (RuntimeException e) {
            e.printStackTrace();
            tx.rollback();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }
}