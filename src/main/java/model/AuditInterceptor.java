package model;

import org.hibernate.EmptyInterceptor;
import org.hibernate.type.Type;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.io.Serializable;

/**
 * Created by Shrey on 12/30/2015.
 */
public class AuditInterceptor extends EmptyInterceptor {

    @Override
    public boolean onSave(
            Object entity,
            Serializable id,
            Object[] state,
            String[] propertyNames,
            Type[] types) {

        if (entity instanceof Item) {
            for(int i = 0; i < propertyNames.length; i++) {
                if (propertyNames[i].equals("sysCreateTime")) {
                    state[i] = DateTime.now().withZone(DateTimeZone.UTC);
                }
            }
            return true;
        } else if (entity instanceof NewsLink) {
            NewsLink newsLink = (NewsLink) entity;
            for(int i = 0; i < propertyNames.length; i++) {
                if (propertyNames[i].equals("category")) {
                    if (newsLink.getCategory() == null || newsLink.getCategory().isEmpty()) {
                        state[i] = newsLink.getItem().getCategory();
                    }
                } else if (propertyNames[i].equals("description")) {
                    state[i] = newsLink.getItem().getDescription();
                }
            }
            return true;
        }
        return false;
    }
}
