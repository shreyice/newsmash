package model;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Shrey on 12/24/2015.
 */

@Entity
@Table(name = "rssItem", uniqueConstraints = {@UniqueConstraint(columnNames =  "title")})
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true, nullable = false)
    private int id;

    @Column(name = "title", nullable = false )
    private String title;

    @Column(name = "rss_type", nullable = false )
    private String type;

    @Column(name = "url", unique = true, nullable = false )
    private String newsUrl;

    @Column (name = "category" )
    private String category;

    @Column (name = "description")
    private String description;

    @Column (name = "pub_date")
    @Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime publicationDate;

    @Column (name = "src")
    private String src;

    @Column (name = "sys_create_time")
    @Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime sysCreateTime;

    @OneToMany (fetch = FetchType.LAZY, mappedBy = "item", cascade = CascadeType.ALL)
    private List<NewsLink> newsLinks;


    public Item() {
        this.newsLinks = new ArrayList<>();
        this.sysCreateTime = new DateTime(1972, 12, 3, 0, 0, 0, 0);
    }

    public List<NewsLink> getNewsLinks() {
        return newsLinks;
    }

    public void setNewsLinks(List<NewsLink> newsLinks) {
        this.newsLinks = newsLinks;
    }

    public void setNewsLink(NewsLink newsLink) {
        newsLink.setItem(this);
        this.newsLinks.add(newsLink);
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNewsUrl() {
        return newsUrl;
    }

    public void setNewsUrl(String newsUrl) {
        this.newsUrl = newsUrl;
    }

    public DateTime getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(String publicationDate) {

        DateTime dt;
        DateTimeFormatter formatter;
        try {
            formatter = DateTimeFormat.forPattern("EEE, dd MMM yyyy HH:mm:ss zzz");
            dt = formatter.parseDateTime(publicationDate);

        } catch (Exception e) {
            formatter = DateTimeFormat.forPattern("EEE, dd MMM yyyy HH:mm:ss Z");
            dt = formatter.parseDateTime(publicationDate);
        }
        this.publicationDate = dt.withZone(DateTimeZone.UTC);

    }

    public void setPublicationDate(DateTime publicationDate) {
        this.publicationDate = publicationDate.withZone(DateTimeZone.UTC);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public DateTime getSysCreateTime() {
        return sysCreateTime;
    }

    public void setSysCreateTime(DateTime sysCreateTime) {
        this.sysCreateTime = sysCreateTime.withZone(DateTimeZone.UTC);
    }

}
