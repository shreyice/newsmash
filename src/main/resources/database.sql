CREATE DATABASE rssFeed;
USE rssFeed;
    CREATE TABLE rssFeed (
        id INT UNSIGNED NOT NULL AUTO_INCREMENT,
        PRIMARY KEY (id),
        rss_news_url     	    TEXT NOT NULL,
        rss_category  		    TEXT,
        news_video_url_swf	    TEXT,
        news_video_url_mp4	    TEXT,
        news_category  		    TEXT,
        news_keywords		    TEXT,
        title     			    VARCHAR(283) NOT NULL UNIQUE,
        rss_description 	    TEXT,
        news_url_description 	TEXT,
        pub_date 		        TIMESTAMP NOT NULL,
        duration                TEXT,
        html_src_news_link       LONGBLOB,
        news_src                     TEXT
    );
