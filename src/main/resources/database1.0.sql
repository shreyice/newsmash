	CREATE DATABASE rssFeed;
	USE rssFeed;

	CREATE TABLE rssItem (
		id INT UNSIGNED NOT NULL AUTO_INCREMENT,
		url TEXT NOT NULL,
		category TEXT,
		keywords TEXT,
		title VARCHAR(283) NOT NULL UNIQUE,
		rss_type VARCHAR(10),
		description TEXT,
		pub_date TIMESTAMP NOT NULL,
		src  VARCHAR(20),
		sys_create_time TIMESTAMP NOT NULL,
		PRIMARY KEY (id)
	);

	CREATE TABLE newsLink (
		id INT UNSIGNED NOT NULL AUTO_INCREMENT,
		src  VARCHAR(20),
		item_id INT UNSIGNED,	
		title VARCHAR(283) unique,
		news_link_type VARCHAR(10),
		video_url_swf TEXT,	
		video_url_mp4 TEXT,
		category TEXT,
		keywords TEXT,
		description TEXT,
		pub_date TIMESTAMP NOT NULL,
		duration INT,
		html_src LONGBLOB,
		PRIMARY KEY (id),
		FOREIGN KEY (item_id)
			REFERENCES rssItem (id)
			on delete cascade
	);	